package common.config;

import common.constant.CashConst;
import common.constant.FieldConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.CacheErrorHandler;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

/**
 * redis配置类
 *
 * @author zys
 */
@Slf4j
@EnableCaching
@Configuration
public class RedisConfig extends CachingConfigurerSupport {

    static {
        log.info("Init -> [{}]", "RedisConfig");
    }

    @Lazy
    @Resource
    private LettuceConnectionFactory lettuceConnectionFactory;

    @Bean
    @Override
    public KeyGenerator keyGenerator() {
        //设置自动key的生成规则
        return (target, method, params) -> {
            StringBuilder key = new StringBuilder();
            key.append(target.getClass().getName());
            key.append(":");
            key.append(method.getName());
            for (Object obj : params) {
                key.append(":").append(obj);
            }
            return key.toString();
        };
    }

    @Bean
    public RedisCacheWriter writer() {
        return RedisCacheWriter.nonLockingRedisCacheWriter(lettuceConnectionFactory);
    }

    @Override
    @Bean("cacheManager")
    public CacheManager cacheManager() {
        // 初始化缓存管理器
        Map<String, RedisCacheConfiguration> mapCacheConfigurations = new HashMap<>(FieldConst.EIGHT);
        StringRedisSerializer stringSerializer = keySerializer();
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer = valueSerializer();
        log.info("Init -> [{}]", "CacheManager RedisCacheManager Start");
        // cache service 组织 储存 300s
        mapCacheConfigurations.put(CashConst.ORG,
            configuration(new StringRedisSerializer(), fastJsonRedisSerializer, FieldConst.ORG_CACHE_TIME));
        return RedisCacheManager.builder(writer()).initialCacheNames(mapCacheConfigurations.keySet())
            .withInitialCacheConfigurations(mapCacheConfigurations)
            .cacheDefaults(configuration(stringSerializer, fastJsonRedisSerializer, FieldConst.DEFAULT_CACHE_TIME))
            .build();
    }

    private RedisCacheConfiguration configuration(StringRedisSerializer stringSerializer,
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer, Integer time) {
        return RedisCacheConfiguration.defaultCacheConfig()
            .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(stringSerializer))
            .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(fastJsonRedisSerializer))
            .disableCachingNullValues().entryTtl(Duration.ofSeconds(time));
    }

    @Bean("redisTemplate")
    public RedisTemplate<String, Object> redisTemplate() {
        log.info("Init -> [{}]", "RedisTemplate ValueSerializer For FastJsonRedisSerializer");
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(lettuceConnectionFactory);
        StringRedisSerializer stringSerializer = keySerializer();
        FastJsonRedisSerializer<Object> fastJsonRedisSerializer = valueSerializer();
        // key序列化
        redisTemplate.setKeySerializer(stringSerializer);
        // value序列化
        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
        // Hash key序列化
        redisTemplate.setHashKeySerializer(stringSerializer);
        // Hash value序列化
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    private StringRedisSerializer keySerializer() {
        return new StringRedisSerializer();
    }

    private FastJsonRedisSerializer<Object> valueSerializer() {
        return new FastJsonRedisSerializer<>(Object.class);
    }

    @Bean
    @Override
    public CacheErrorHandler errorHandler() {
        // 异常处理，当Redis发生异常时，打印日志
        log.info("Init -> [{}]", "Redis CacheErrorHandler");
        return new CacheErrorHandler() {

            @Override
            public void handleCacheGetError(RuntimeException e, Cache cache, Object key) {
                log.error("Redis occur handleCacheGetError：key -> [{}]", key, e);
            }

            @Override
            public void handleCachePutError(RuntimeException e, Cache cache, Object key, Object value) {
                log.error("Redis occur handleCachePutError：key -> [{}]；value -> [{}]", key, value, e);
            }

            @Override
            public void handleCacheEvictError(RuntimeException e, Cache cache, Object key) {
                log.error("Redis occur handleCacheEvictError：key -> [{}]", key, e);
            }

            @Override
            public void handleCacheClearError(RuntimeException e, Cache cache) {
                log.error("Redis occur handleCacheClearError：", e);
            }

        };
    }

}
