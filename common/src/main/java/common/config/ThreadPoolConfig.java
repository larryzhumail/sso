package common.config;

import common.constant.FieldConst;
import common.constant.NameConst;
import common.custom.dto.MsgDelay;
import common.task.ThreadPoolModel;
import common.task.ThreadPoolModelValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.concurrent.DelayQueue;

/**
 * @author zys
 * @since 2023-05-13
 */
@Slf4j
@EnableAsync
@Configuration
@EnableConfigurationProperties(ThreadPoolModelValue.class)
@ConditionalOnProperty(name = NameConst.THREAD_POOL_ENABLE, havingValue = FieldConst.TRUE_STR)
public class ThreadPoolConfig {

    /** {@link ThreadPoolModelValue 线程池静态配置信息} */
    private final ThreadPoolModelValue threadPoolModelValue;
    
    @Resource
    ConfigurableBeanFactory configurableBeanFactory;

    public ThreadPoolConfig(ThreadPoolModelValue threadPoolModelValue) {
        this.threadPoolModelValue = threadPoolModelValue;
    }

    @Bean(name = NameConst.MSG_QUEUE)
    public DelayQueue<MsgDelay> msgDelayQueue() {
        log.info("Init DelayQueue -> [MsgDelayQueue]");
        return new DelayQueue<>();
    }

    @PostConstruct
    public void init() {
        log.info("Init -> [MultiThreadPool]");
        for (ThreadPoolModel pool : threadPoolModelValue.getPoolLists()) {
            ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();

            //设置线程池静态参数信息
            taskExecutor.setCorePoolSize(pool.getCoreSize());
            taskExecutor.setMaxPoolSize(pool.getMaxSize());
            taskExecutor.setQueueCapacity(pool.getQueueCapacity());
            taskExecutor.setThreadNamePrefix(pool.getPoolId() + ":");
            taskExecutor.setWaitForTasksToCompleteOnShutdown(true);

            //注册Spring Bean
            configurableBeanFactory.registerSingleton(pool.getPoolId(), taskExecutor);
            log.info("Init -> 线程池注册 -> [{}]", pool.getPoolId());

            //初始化线程池
            taskExecutor.initialize();
        }
    }

}
