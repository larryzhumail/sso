package common.cache;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 再说一次服务。
 * redis通用API帮助接口
 *
 * @author zys
 * @date 2023/04/25
 */
@SuppressWarnings("unused")
public interface RedisService {

    /**
     * 判断一个key是否存在
     *
     * @param key 钥匙
     * @return boolean
     */
    boolean exists(String key);

    /**
     * 期满
     * 根据key指定剩余时间,可以指定时间单位
     *
     * @param key 钥匙
     * @param time 时间
     * @param unit 单位
     * @return boolean
     */
    boolean expire(String key, long time, TimeUnit unit);

    /**
     * 获取到期时间
     * 根据key获取剩余时间
     * 可以指定时间单位
     *
     * @param key 钥匙
     * @param unit 单位
     * @return long
     */
    long getExpire(String key, TimeUnit unit);

    /**
     * 普通获取
     *
     * @param key 钥匙
     * @return {@link T}
     */
    <T> T get(String key);

    /**
     * 获取整型
     *
     * @param key 钥匙
     * @return {@link Integer}
     */
    Integer getInt(String key);

    /**
     * 集
     * 普通放入
     *
     * @param key 钥匙
     * @param val VAL
     * @return boolean
     */
    boolean set(String key, Object val);

    /**
     * 集
     * 普通放入并设置时间,
     * 可设置时间单位
     *
     * @param key 钥匙
     * @param val VAL
     * @param time 时间
     * @param unit 单位
     * @return boolean
     */
    boolean set(String key, Object val, long time, TimeUnit unit);

    /**
     * 如果不在，则设置
     * 尝试放入一个key并设置时间
     * 如果该key不存在,则成功放入并返回true
     * 如果该key已存在,则调用无效并返回false
     *
     * @param key 钥匙
     * @param val VAL
     * @param time 时间
     * @param unit 单位
     * @return boolean
     */
    boolean setIfAbsent(String key, Object val, long time, TimeUnit unit);

    /**
     * 密钥集
     * 模糊查询key,传 * 查所有,仅支持正则
     *
     * @param pattern 图案
     * @return {@link Set}<{@link String}>
     */
    Set<String> keySet(String pattern);

    /**
     * 德尔
     * 删除一个key
     *
     * @param key 钥匙
     * @return boolean
     */
    boolean del(String key);

    /**
     * 删除多个key
     *
     * @param keys 钥匙
     * @return long
     */
    long del(Set<String> keys);

    /**
     * 铬
     * 递增/递减一个key的val
     *
     * @param delta 正值为递增跨度,负值为递减跨度
     * @param key 钥匙
     * @return long
     */
    long cr(String key, long delta);

    /**
     * 铬
     *
     * @param key 钥匙
     * @param delta 德尔塔
     * @param time 时间
     * @param unit 单位
     * @return long
     */
    long cr(String key, long delta, long time, TimeUnit unit);

    /**
     * 人类存在
     * 判断hash中是否有某项
     *
     * @param key 钥匙
     * @param item 项目
     * @return boolean
     */
    boolean hexists(String key, String item);

    /**
     * 获取hash某项的值
     *
     * @param key 钥匙
     * @param item 项目
     * @return {@link T}
     */
    <T> T hget(String key, String item);

    /**
     * 提示整型
     *
     * @param key 钥匙
     * @param item 项目
     * @return {@link Integer}
     */
    Integer hgetInt(String key, String item);

    /**
     * Hset
     * 向一张hash表中放入数据,如果不存在将创建
     *
     * @param key 钥匙
     * @param item 项目
     * @param val VAL
     * @return boolean
     */
    boolean hset(String key, String item, Object val);

    /**
     * Hset
     * 向一张hash表中放入数据,如果不存在将创建
     * 设置有效时间,可指定时间单位
     *
     * @param key 钥匙
     * @param item 项目
     * @param val VAL
     * @param time 时间
     * @param unit 单位
     * @return boolean
     */
    boolean hset(String key, String item, Object val, long time, TimeUnit unit);

    /**
     * Hdel
     * 单个/批量删除hash表中的值
     *
     * @param key 钥匙
     * @param item 项目
     * @return long
     */
    long hdel(String key, Object... item);

    /**
     * HCR
     * hash递增/递减,
     * 如果不存在,就会创建一个并把新增后的值返回
     *
     * @param key 钥匙
     * @param item 项目
     * @param delta 德尔塔
     * @return double
     */
    double hcr(String key, String item, double delta);

    /**
     * Hkey集
     * 获取某个hash key下的所有item
     *
     * @param key 钥匙
     * @return {@link Set}<{@link T}>
     */
    <T> Set<T> hkeySet(String key);

    /**
     * 天哪。
     * 获取某个hash key下的所有val
     *
     * @param key 钥匙
     * @return {@link List}<{@link T}>
     */
    <T> List<T> hvals(String key);

    /**
     * 哼哼
     * 获取hashKey对应所有键值
     *
     * @param key 钥匙
     * @return {@link Map}<{@link K}, {@link V}>
     */
    <K, V> Map<K, V> hmget(String key);

    /**
     * Hmset
     * 放入一个hash
     *
     * @param key 钥匙
     * @param map 地图
     * @return boolean
     */
    boolean hmset(String key, Map<Object, Object> map);

    /**
     * Hmset
     * 放入一个hash并设置有效时间
     * 可指定时间单位
     *
     * @param key 钥匙
     * @param map 地图
     * @param time 时间
     * @param unit 单位
     * @return boolean
     */
    boolean hmset(String key, Map<Object, Object> map, long time, TimeUnit unit);

    /**
     * 扫描
     * 执行scan命令, 匹配已有的键
     *
     * @param pattern 图案
     * @return {@link Set}<{@link String}>
     */
    Set<String> scan(String pattern);

}
