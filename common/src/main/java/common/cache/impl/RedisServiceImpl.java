package common.cache.impl;

import common.cache.RedisService;
import common.constant.NameConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 实施Redis服务
 *
 * @author zys
 * @date 2023/04/25
 */
@Slf4j
@Service(NameConst.REDIS_SERVICE)
@SuppressWarnings("unchecked")
public class RedisServiceImpl implements RedisService {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Override
    public boolean exists(String key) {
        Boolean hasKey = redisTemplate.hasKey(key);
        return hasKey != null && hasKey;
    }

    @Override
    public boolean expire(String key, long time, TimeUnit unit) {
        Boolean expire = redisTemplate.expire(key, time, unit);
        return expire != null && expire;
    }

    @Override
    public long getExpire(String key, TimeUnit unit) {
        Long expire = redisTemplate.getExpire(key, unit);
        return expire == null ? -1 : expire;
    }

    @Override
    public <T> T get(String key) {
        Object o = redisTemplate.opsForValue().get(key);
        return o == null ? null : (T)o;
    }

    @Override
    public Integer getInt(String key) {
        Object o = redisTemplate.opsForValue().get(key);
        return o == null ? null : Integer.parseInt(o.toString());
    }

    @Override
    public boolean set(String key, Object val) {
        try {
            redisTemplate.opsForValue().set(key, val);
            return true;
        } catch (Exception e) {
            log.error("Set Redis Key Throw Exception: ", e);
            return false;
        }
    }

    @Override
    public boolean set(String key, Object val, long time, TimeUnit unit) {
        try {
            redisTemplate.opsForValue().set(key, val, time, unit);
            return true;
        } catch (Exception e) {
            log.error("Set Redis Key Throw Exception: ", e);
            return false;
        }
    }

    @Override
    public boolean setIfAbsent(String key, Object val, long time, TimeUnit unit) {
        Boolean absent = redisTemplate.opsForValue().setIfAbsent(key, val, time, unit);
        return absent != null && absent;
    }

    @Override
    public Set<String> keySet(String pattern) {
        return redisTemplate.keys(pattern);
    }

    @Override
    public boolean del(String key) {
        Boolean del = redisTemplate.delete(key);
        return del != null && del;
    }

    @Override
    public long del(Set<String> keys) {
        Long del = redisTemplate.delete(keys);
        return del == null ? 0 : del;
    }

    @Override
    public long cr(String key, long delta) {
        Long cr = redisTemplate.opsForValue().increment(key, delta);
        return cr == null ? -1 : cr;
    }

    @Override
    public long cr(String key, long delta, long time, TimeUnit unit) {
        long cr = cr(key, delta);
        expire(key, time, unit);
        return cr;
    }

    @Override
    public boolean hexists(String key, String item) {
        return redisTemplate.opsForHash().hasKey(key, item);
    }

    @Override
    public <T> T hget(String key, String item) {
        Object o = redisTemplate.opsForHash().get(key, item);
        return o == null ? null : (T)o;
    }

    @Override
    public Integer hgetInt(String key, String item) {
        Object o = redisTemplate.opsForHash().get(key, item);
        return o == null ? null : Integer.parseInt(o.toString());
    }

    @Override
    public boolean hset(String key, String item, Object val) {
        try {
            redisTemplate.opsForHash().put(key, item, val);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean hset(String key, String item, Object val, long time, TimeUnit unit) {
        return hset(key, item, val) && expire(key, time, unit);
    }

    @Override
    public long hdel(String key, Object... item) {
        return redisTemplate.opsForHash().delete(key, item);
    }

    @Override
    public double hcr(String key, String item, double delta) {
        return redisTemplate.opsForHash().increment(key, item, delta);
    }

    @Override
    public <T> Set<T> hkeySet(String key) {
        return redisTemplate.opsForHash().keys(key).stream().map(k -> (T)k).collect(Collectors.toSet());
    }

    @Override
    public <T> List<T> hvals(String key) {
        return (List<T>)redisTemplate.opsForHash().values(key);
    }

    @Override
    public <K, V> Map<K, V> hmget(String key) {
        return (Map<K, V>)redisTemplate.opsForHash().entries(key);
    }

    @Override
    public boolean hmset(String key, Map<Object, Object> map) {
        try {
            redisTemplate.opsForHash().putAll(key, map);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean hmset(String key, Map<Object, Object> map, long time, TimeUnit unit) {
        return hmset(key, map) && expire(key, time, unit);
    }

    @Override
    public Set<String> scan(String pattern) {
        return redisTemplate.execute((RedisCallback<Set<String>>)connection -> {
            Set<String> keysTem = new HashSet<>();
            Cursor<byte[]> cursor = connection.scan(ScanOptions.scanOptions().match(pattern).count(10000).build());
            while (cursor.hasNext()) {
                keysTem.add(new String(cursor.next(), StandardCharsets.UTF_8));
            }
            return keysTem;
        });
    }

}
