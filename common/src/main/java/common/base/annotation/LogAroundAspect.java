package common.base.annotation;

import cn.hutool.core.util.IdUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;

/**
 * @author KaiLo
 * @version 1.0
 * @description: LogAroundAspect 测试阶段输出出参入参
 * @date 2021/6/11 17:53
 */
@Slf4j
@Aspect
@Component
public class LogAroundAspect {

    @Pointcut("@annotation(common.base.annotation.LogAround)")
    public void logAroundPointcut() {}

    @Around("logAroundPointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        // 本次执行启示时间
        long enterTime = System.currentTimeMillis();
        // 本次执行UUID
        String id = IdUtil.fastUUID();
        String uuid = id.substring(0, 5);
        // 准备信息
        MethodSignature methodSignature = (MethodSignature)joinPoint.getSignature();
        LogAround logAround = methodSignature.getMethod().getAnnotation(LogAround.class);
        String methodName = methodSignature.getName();
        String[] argNames = methodSignature.getParameterNames();
        Object[] args = joinPoint.getArgs();
        StringJoiner joiner = new StringJoiner(", ");
        for (int i = 0; i < argNames.length; i++) {
            joiner.add(argNames[i] + ":" + JSON.toJSONString(args[i], SerializerFeature.WriteMapNullValue));
        }
        String path = joinPoint.getTarget().getClass().getSimpleName() + "." + methodName;

        if (logAround.inputLog()) {
            System.out.println();
            log.info("==> Enter `{}` [{}], Input: [{}]", path, uuid, joiner);
        }

        long spendTime;
        try {
            Object proceed = joinPoint.proceed();
            if (logAround.outputLog()) {
                spendTime = System.currentTimeMillis() - enterTime;
                String result = JSON.toJSONString(proceed);
                log.info("<== Exit  `{}` [{}]:({}ms), Output: \n{}\n", path, uuid, spendTime, result);
            }
            return proceed;
        } catch (Exception cause) {
            if (logAround.outputLog()) {
                spendTime = System.currentTimeMillis() - enterTime;
                log.error("<== ERROR `{}` [{}]:({}ms), ERROR: {}\n", path, uuid, spendTime, cause.getMessage());
            }
            throw cause;
        }
    }

}
