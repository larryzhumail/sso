package common.base.annotation;

import java.lang.annotation.*;

/**
 * @author KaiLo
 * @version 1.0
 * @description: LogAround
 * @date 2021/6/11 17:51
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface LogAround {

    boolean inputLog() default true;

    boolean outputLog() default true;

}
