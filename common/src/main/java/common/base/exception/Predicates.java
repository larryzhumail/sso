package common.base.exception;

import cn.hutool.core.util.StrUtil;

/**
 * @author zys
 * @since 2023-05-05
 */
@SuppressWarnings("unused")
public class Predicates {

    public static void check(Boolean expression, String message) {
        if (expression) {
            throw new ApiException(message);
        }
    }

    public static void check(Boolean expression, String template, Object... params) {
        if (expression) {
            throw new ApiException(StrUtil.format(template, params));
        }
    }

}
