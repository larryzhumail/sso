package common.base.exception;

/**
 * 自定义例外
 *
 * @author zys
 * @date 2022/12/12
 * @since 2022-12-12
 */
@SuppressWarnings("unused")
public class ApiException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private final String message;
    private Integer code;

    public ApiException(String message) {
        this.code = 500; this.message = message;
    }

    public ApiException(String message, Integer code) {
        this.code = code; this.message = message;
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause); this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}