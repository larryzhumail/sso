package common.base.result;

import common.constant.FieldConst;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 返回的单个实体统一格式
 *
 * @author zys
 */
@Data
@ApiModel(value = "BaseResultData对象", description = "返回的单个实体统一格式")
public class BaseResultData<T> {

    @ApiModelProperty(value = "请求状态")
    private Integer code;
    @ApiModelProperty(value = "请求结果说明")
    private String msg;
    @ApiModelProperty(value = "返回数据")
    private T data;

    private BaseResultData() {
        this.code = FieldConst.SUCCESS_CODE;
        this.msg  = FieldConst.SUCCESS_STR;
    }

    private BaseResultData(T data) {
        this();
        this.data = data;
    }

    public BaseResultData(int code, String msg, T data) {
        this.code = code;
        this.msg  = msg;
        this.data = data;
    }

    public static <T> BaseResultData<T> success(T data) {
        return new BaseResultData<>(data);
    }

    public static <T> BaseResultData<T> fail(int code, String msg, T data) {
        return new BaseResultData<>(code, msg, data);
    }

}
