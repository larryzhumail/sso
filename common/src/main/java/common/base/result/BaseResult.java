package common.base.result;

import common.constant.FieldConst;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 数据返回
 *
 * @author zys
 * @date 2023/04/18
 */
@Data
@ApiModel(value = "BaseResult对象", description = "无夹带数据返回时的统一格式")
public class BaseResult {

    @ApiModelProperty(value = "请求状态")
    private Integer code;
    @ApiModelProperty(value = "请求结果说明")
    private String msg;

    private BaseResult() {
        this.code = FieldConst.SUCCESS_CODE;
        this.msg  = FieldConst.SUCCESS_STR;
    }

    private BaseResult(int code, String msg) {
        this.code = code;
        this.msg  = msg;
    }

    public static BaseResult success() {
        return new BaseResult();
    }

    public static BaseResult fail(int code, String msg) {
        return new BaseResult(code, msg);
    }

}
