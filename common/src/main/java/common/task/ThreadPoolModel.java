package common.task;

import lombok.Data;

/**
 * @author zys
 * @since 2023-05-13
 */
@Data
public class ThreadPoolModel {

    //#region 静态参数配置
    /** 线程池唯一标识(英文) */
    private String poolId;

    /** 线程池名称(中文描述) */
    private String poolName;

    /** 初始线程数 */
    private Integer coreSize;

    /** 最大线程数 */
    private Integer maxSize;

    /** 等待队列容量 */
    private Integer queueCapacity;
    //#endregion

}
