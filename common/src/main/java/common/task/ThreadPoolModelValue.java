package common.task;

import common.constant.NameConst;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @author zys
 * @since 2023-05-13
 */
@Data
@ConfigurationProperties(NameConst.THREAD_POOL_CONFIG_NAME)
public class ThreadPoolModelValue {

    private Boolean enable;

    private List<ThreadPoolModel> poolLists;

}
