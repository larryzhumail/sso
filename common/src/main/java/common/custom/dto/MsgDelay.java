package common.custom.dto;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * @author zys
 * @since 2023-05-11
 */
@Getter
@AllArgsConstructor
public class MsgDelay implements Delayed {

    private static final TimeUnit SOURCE = TimeUnit.MILLISECONDS;
    private final long msgId;
    private final long delayTime;

    public String getExecuteTime() {
        return new DateTime(delayTime + 1000).toMsStr();
    }

    @Override
    public long getDelay(TimeUnit unit) {
        //这里加两秒是为了避免出现事务未提交导致消费冲突的情况
        long duration = (delayTime + 1000) - System.currentTimeMillis();
        return unit.convert(duration, SOURCE);
    }

    @Override
    public int compareTo(Delayed obj) {
        return Long.compare(this.delayTime, ((MsgDelay)obj).delayTime);
    }

    @Override
    public String toString() {
        return "MsgDelay{" + "sendId=" + msgId + ", delayTime=" + getExecuteTime() + "}";
    }

}
