package common.custom.form;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author zys
 * @since 2023-05-05
 */
@Data
@ApiModel("组织请求体")
public class InfoOrgForm {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "父级ID")
    private Long parentId;

    @ApiModelProperty(value = "名称")
    private String name;

}
