package common.custom.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author zys
 * @since 2023-05-06
 */
@Data
@ApiModel(value = "OrgTreeNode组织树", description = "组织树")
public class InfoOrgTreeNode {

    @ApiModelProperty("ID")
    private Long id;

    @ApiModelProperty("父辈ID")
    private Long orgParentId;

    @ApiModelProperty("名称-本部门")
    private String orgName;

    @ApiModelProperty("名称-部门全路径 (使用`-`隔开)")
    private String orgFullName;

    @ApiModelProperty("父辈ID列表 (使用`,`隔开)")
    private String orgFullParent;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty("子辈")
    private List<InfoOrgTreeNode> children;

}
