package common.custom.vo;

import lombok.Data;

/**
 * @author zys
 * @since 2023-04-22
 */
@Data
@SuppressWarnings("unused")
public class InfoUserVo {}
