package common.constant;

/**
 * @author zys
 * @since 2023-04-19
 */
@SuppressWarnings("unused")
public class FieldConst {

    public static final String TRUE_STR = "true";
    public static final String FALSE_STR = "false";

    /* BaseResult */

    public static final Integer SUCCESS_CODE = 200;
    public static final String SUCCESS_STR = "SUCCESS";
    public static final String FAIL_STR = "FAIL";

    /*一些分割*/

    public static final String EMPTY = "";
    public static final String DASH = "-";
    public static final String COMMA = ",";

    /*数字*/

    public static final Integer ONE = 1;
    public static final Integer TWO = 2;
    public static final Integer THREE = 3;
    public static final Integer FOUR = 4;
    public static final Integer FIVE = 5;
    public static final Integer SIX = 6;
    public static final Integer SEVEN = 7;
    public static final Integer EIGHT = 8;
    public static final Integer NINE = 9;

    /*缓存时间s*/

    public static final Integer DEFAULT_CACHE_TIME = 30;
    public static final Integer ORG_CACHE_TIME = 300;

}
