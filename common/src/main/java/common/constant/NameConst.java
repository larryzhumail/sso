package common.constant;

/**
 * 注入类名称常量 统一管理
 *
 * @author zys
 * @date 2023/05/12
 * @since 2023-05-07
 */
public class NameConst {

    //#region tool 工具类

    public static final String ID_TOOL = "idTool";
    public static final String MAPPER_PACKAGE_PATH = "api.database.mapper";

    //#endregion
    //#region value application 读取变量

    public static final String TASK_PREFIX = "task-enable";
    public static final String TASK_VALUE = "taskValue";

    //#endregion
    //#region MsgDelayQueue 信息延迟队列 所有注入（EXECUTOR 一个一用）

    public static final String MSG_EXECUTOR = "msg-task";
    public static final String MSG_QUEUE = "msgDelayQueue";
    public static final String MSG_TASK = "msgDelayQueueTask";

    //#endregion
    //#region 配置注入值

    public static final String THREAD_POOL_ENABLE = "thread-pool.enable";
    public static final String THREAD_POOL_CONFIG_NAME = "thread-pool";

    //#endregion
    //#region service

    public static final String BASE_TOOLS_SERVICE = "baseToolsService";
    public static final String REDIS_SERVICE = "redisService";
    public static final String CACHE_SERVICE = "cacheService";
    public static final String MSG_SERVICE = "msgService";

    public static final String ORG_SERVICE = "orgService";
    public static final String USER_SERVICE = "userService";

    //#endregion

}
