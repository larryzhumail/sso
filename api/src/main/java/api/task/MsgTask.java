package api.task;

import api.configuration.value.TaskValue;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.ObjectUtil;
import common.constant.NameConst;
import common.custom.dto.MsgDelay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.DelayQueue;

/**
 * 消息任务
 *
 * @author zys
 * @date 2023/05/13
 * @since 2023-05-12
 */
@Slf4j
@Component(NameConst.MSG_TASK)
public class MsgTask {

    @Resource(name = NameConst.TASK_VALUE)
    private TaskValue taskEnable;
    @Resource(name = NameConst.MSG_QUEUE)
    private DelayQueue<MsgDelay> msgDelayQueue;

    @Async(NameConst.MSG_EXECUTOR)
    public void start() {
        log.info("Init -> [MsgTask]");
        init();
        while (taskEnable.getMsg()) {
            try {
                MsgDelay task = msgDelayQueue.take();
                if (ObjectUtil.isNotEmpty(taskEnable)) {
                    Long sendId = task.getMsgId();
                    log.info("<======== 开始处理ID为[{}]计划在[{}]处理的消息 ========>", sendId, task.getExecuteTime());
                }
            } catch (InterruptedException e) {
                log.error("信息处理从延时队列中取数失败！", e);
            }
        }
        log.info("End  -> [信息任务未开启-信息监听停止-清除信息缓存]");
        while (!taskEnable.getMsg()) {
            msgDelayQueue.clear();
            ThreadUtil.safeSleep(5000L);
        }
    }

    /**
     * 初始化队列 -> 待调整，调整为从数据库（持久层）拿未发送信息放入列表
     */
    private void init() {
        log.info("Init -> [初始化 放入一个测试信息, ID为(0)]");
        msgDelayQueue.offer(new MsgDelay(0L, System.currentTimeMillis() + 1000));
    }

}
