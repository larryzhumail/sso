package api.service.impl;

import api.database.base.BaseServiceImpl;
import api.database.entity.UserPo;
import api.database.mapper.UserMapper;
import api.service.UserService;
import common.constant.NameConst;
import common.custom.dto.MsgDelay;
import common.custom.vo.InfoUserVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.DelayQueue;

/**
 * @author zys
 * @since 2023-04-22
 */
@Slf4j
@Service(NameConst.USER_SERVICE)
public class UserServiceImpl extends BaseServiceImpl<UserMapper, UserPo> implements UserService {

    @Override
    public List<InfoUserVo> getUsers() {
        return null;
    }

}
