package api.service.impl;

import api.database.base.BaseServiceImpl;
import api.database.entity.OrgPo;
import api.database.mapper.OrgMapper;
import api.service.OrgService;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import common.base.exception.Predicates;
import common.constant.NameConst;
import common.custom.form.InfoOrgForm;
import common.custom.vo.InfoOrgTreeNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static common.constant.FieldConst.*;

/**
 * @author zys
 * @since 2023-05-05
 */
@Slf4j
@Service(NameConst.ORG_SERVICE)
public class OrgServiceImpl extends BaseServiceImpl<OrgMapper, OrgPo> implements OrgService {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long saveOrg(InfoOrgForm form) {
        /*查询是否重名*/
        Long count = queryWrapper().eq(OrgPo::getOrgName, form.getName())
            .eq(ObjectUtil.isEmpty(form.getParentId()), OrgPo::getOrgParentId, 0L)
            .eq(ObjectUtil.isNotEmpty(form.getParentId()), OrgPo::getOrgParentId, form.getParentId()).count();
        Predicates.check(count > 0, "当前组织层级已有次名称");
        /*检测是否有父辈组织*/
        OrgPo parent = topPo();
        if (ObjectUtil.isNotEmpty(form.getParentId())) {
            parent = getById(form.getParentId());
            Predicates.check(ObjectUtil.isEmpty(parent), "当前组织父辈无效或已删除");
        }
        /*拼接新组织到父辈组织*/
        OrgPo orgPo = new OrgPo();
        setParent(orgPo, parent, form.getName());
        /*保存*/
        Predicates.check(!save(orgPo), "保存组织失败！");
        /*清理缓存*/
        tools.getCache().cleanOrgCache();
        return orgPo.getId();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long deleteOrg(InfoOrgForm form) {
        Predicates.check(ObjectUtil.isEmpty(form.getId()), "输入删除ID");
        List<OrgPo> allLowers = getAllLowers(form.getId());
        String message = "无法删除，该ID下还有其他组织 -> " +
                         allLowers.stream().map(OrgPo::getOrgFullName).collect(Collectors.toList());
        Predicates.check(allLowers.size() > 0, message);
        Predicates.check(!removeById(form.getId()), "删除失败");
        /*清理缓存*/
        tools.getCache().cleanOrgCache();
        return form.getId();
    }

    @Override
    public List<InfoOrgTreeNode> getTree() {
        Map<Long, List<OrgPo>> orgMap =
            tools.getCache().allOrgList().stream().collect(Collectors.groupingBy(OrgPo::getOrgParentId));
        List<InfoOrgTreeNode> topNodeList = new ArrayList<>();
        buildTree(0L, orgMap, topNodeList);
        return topNodeList;
    }

    /**
     * 构建树
     *
     * @param id 上级ID(全树从0开始)
     * @param orgMap 组织图 (key->ParentId)
     * @param nodeList 节点列表
     */
    private void buildTree(Long id, Map<Long, List<OrgPo>> orgMap, List<InfoOrgTreeNode> nodeList) {
        for (OrgPo orgPo : orgMap.getOrDefault(id, new ArrayList<>())) {
            InfoOrgTreeNode treeNode = BeanUtil.toBean(orgPo, InfoOrgTreeNode.class);
            treeNode.setChildren(new ArrayList<>());
            nodeList.add(treeNode);
            buildTree(orgPo.getId(), orgMap, treeNode.getChildren());
        }
    }

    private List<OrgPo> getAllLowers(Long upperPoId) {
        ArrayList<Long> matchedIds = new ArrayList<>();
        matchedIds.add(upperPoId);
        return tools.getCache().allOrgList().stream().sorted().filter(org -> {
            if (matchedIds.contains(org.getOrgParentId())) {
                matchedIds.add(org.getId());
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());
    }

    private void setParent(OrgPo orgPo, OrgPo parent, String name) {
        /*设置组织名+父辈ID*/
        name = name.replace(DASH, EMPTY);
        orgPo.setOrgName(name);
        orgPo.setOrgParentId(parent.getId());
        /*设置部门全路径*/
        String fullName = parent.getOrgFullName() + DASH + name;
        orgPo.setOrgFullName(fullName.startsWith(DASH) ? fullName.substring(ONE) : fullName);
        /*设置父辈ID列表*/
        String fullParent = parent.getOrgFullParent() + COMMA + parent.getId();
        orgPo.setOrgFullParent(fullParent.startsWith(COMMA) ? fullParent.substring(ONE) : fullParent);
    }

    private OrgPo topPo() {
        OrgPo po = new OrgPo();
        po.setId(0L);
        po.setOrgFullName("");
        po.setOrgFullParent("");
        return po;
    }

}
