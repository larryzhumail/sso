package api.service;

import api.database.base.BaseService;
import api.database.entity.OrgPo;
import common.custom.form.InfoOrgForm;
import common.custom.vo.InfoOrgTreeNode;

import java.util.List;

/**
 * @author zys
 * @since 2023-05-05
 */
public interface OrgService extends BaseService<OrgPo> {

    /**
     * 保存组织
     *
     * @param form 表格
     * @return {@link Long}
     */
    Long saveOrg(InfoOrgForm form);

    /**
     * 删除组织
     *
     * @param form 表格
     * @return {@link Long}
     */
    Long deleteOrg(InfoOrgForm form);

    /**
     * 获取组织树
     *
     * @return {@link Object}
     */
    List<InfoOrgTreeNode> getTree();

}
