package api.service.common;

import api.database.entity.OrgPo;
import api.service.OrgService;
import api.service.UserService;
import common.constant.CashConst;
import common.constant.NameConst;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 统一缓存放入此服务，统一管理
 *
 * @author zys
 * @since 2023-05-06
 */
@Service(NameConst.CACHE_SERVICE)
@SuppressWarnings("unused")
public class AllCacheService {

    @Resource(name = NameConst.ORG_SERVICE)
    private OrgService orgService;
    @Resource(name = NameConst.USER_SERVICE)
    private UserService userService;

    //#region 组织缓存

    @CacheEvict(value = CashConst.ORG, allEntries = true)
    public void cleanOrgCache() {}

    @Cacheable(value = CashConst.ORG, key = "'all'")
    public List<OrgPo> allOrgList() {
        return orgService.queryWrapper().list();
    }

    //#endregion

    //#region 用户缓存

    //#endregion

}
