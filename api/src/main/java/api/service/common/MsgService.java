package api.service.common;

import cn.hutool.core.util.StrUtil;
import common.constant.NameConst;
import common.custom.dto.MsgDelay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.DelayQueue;

/**
 * @author zys
 * @since 2023-07-02
 */
@Slf4j
@Service(NameConst.MSG_SERVICE)
public class MsgService extends BaseToolsService {

    @Resource(name = NameConst.MSG_QUEUE)
    protected DelayQueue<MsgDelay> msgDelayQueue;

    public String addMsg() {
        long msgId = getId().nextNumber().longValue();
        log.info("Msg Id {}", msgId);
        MsgDelay msgDelay = new MsgDelay(msgId, System.currentTimeMillis() + 2000L);
        msgDelayQueue.offer(msgDelay);
        log.info("==> 完成插入ID为[{}]计划在[{}]处理的消息 <==", msgId, msgDelay.getExecuteTime());
        return Long.toString(msgId);
    }

    public List<String> msgList() {
        List<String> joiner = new ArrayList<>();
        for (MsgDelay msgDelay : msgDelayQueue) {
            joiner.add(StrUtil.format("消息ID为[{}],计划执行时间[{}]", msgDelay.getMsgId(), msgDelay.getExecuteTime()));
        }
        return joiner;
    }
}
