package api.service.common;

import api.util.IdTool;
import common.cache.RedisService;
import common.constant.NameConst;
import lombok.Getter;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author zys
 * @since 2023-07-02
 */
@Getter
@Service(NameConst.BASE_TOOLS_SERVICE)
public class BaseToolsService {

    @Resource(name = NameConst.ID_TOOL)
    private IdTool id;
    @Resource(name = NameConst.REDIS_SERVICE)
    private RedisService redis;
    @Resource(name = NameConst.CACHE_SERVICE)
    private AllCacheService cache;

}
