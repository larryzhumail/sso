package api.service;

import api.database.base.BaseService;
import api.database.entity.UserPo;
import common.custom.vo.InfoUserVo;

import java.util.List;

/**
 * @author zys
 * @since 2023-04-22
 */
@SuppressWarnings("unused")
public interface UserService extends BaseService<UserPo> {

    /**
     * 获取用户
     *
     * @return {@link List}<{@link InfoUserVo}>
     */
    List<InfoUserVo> getUsers();

}
