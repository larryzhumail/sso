package api.database.base;

import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author zys
 * @since 2023-04-19
 */
@SuppressWarnings("unused")
public interface BaseService<T extends BaseRelEntity> extends IService<T> {

    /**
     * 获取包装器
     *
     * @return {@link LambdaQueryChainWrapper}<{@link T}>
     */
    LambdaQueryChainWrapper<T> queryWrapper();

    /**
     * 获取包装器
     *
     * @return {@link LambdaQueryChainWrapper}<{@link T}>
     */
    LambdaUpdateChainWrapper<T> updateWrapper();

}
