package api.database.base;

import api.service.common.BaseToolsService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import common.constant.NameConst;

import javax.annotation.Resource;

/**
 * @author zys
 * @since 2023-04-19
 */
@SuppressWarnings("unused")
public abstract class BaseServiceImpl<M extends BaseMapper<T>, T extends BaseRelEntity> extends ServiceImpl<M, T>
    implements BaseService<T> {

    @Resource(name = NameConst.BASE_TOOLS_SERVICE)
    protected BaseToolsService tools;

    @Override
    public LambdaQueryChainWrapper<T> queryWrapper() {
        return new LambdaQueryChainWrapper<>(getBaseMapper());
    }

    @Override
    public LambdaUpdateChainWrapper<T> updateWrapper() {
        return new LambdaUpdateChainWrapper<>(getBaseMapper());
    }

}
