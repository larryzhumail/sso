package api.database.base;

import common.base.result.BaseResult;
import common.base.result.BaseResultData;
import org.springframework.beans.factory.annotation.Autowired;

import static common.constant.FieldConst.FAIL_STR;

/**
 * @author zys
 * @since 2023-05-05
 */
@SuppressWarnings({"unused", "SpringJavaInjectionPointsAutowiringInspection"})
public class BaseController<S extends BaseService<P>, P extends BaseRelEntity> {

    @Autowired
    protected S service;

    protected BaseResult ok() {
        return BaseResult.success();
    }

    protected BaseResult fail(int code, String msg) {
        return BaseResult.fail(code, msg);
    }

    protected <T> BaseResultData<T> ok(T data) {
        return BaseResultData.success(data);
    }

    protected <T> BaseResultData<T> fail(int code, String msg, T data) {
        return BaseResultData.fail(code, msg, data);
    }

    protected <T> BaseResultData<T> fail(int code, T data) {
        return new BaseResultData<>(code, FAIL_STR, data);
    }

}
