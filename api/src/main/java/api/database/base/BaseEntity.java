package api.database.base;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.Date;

/**
 * @author KaiLo
 * @version 1.0
 * @description: BaseEntity
 * @date 2022/1/27 11:45
 */

@Data
@Repository
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BaseEntity extends BaseRelEntity implements Serializable {

    /**
     * 新增时不添加更新时间。若更新，再添加更新时间。默认不删除(false)
     */

    @ApiModelProperty(value = "主键-数据唯一ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    protected Long id;

    @ApiModelProperty(value = "数据状态(0-冻结,1-正常), 具体定义按业务分")
    @TableField(fill = FieldFill.INSERT)
    protected Integer status;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.UPDATE)
    protected Date updateTime;

    @TableLogic
    @ApiModelProperty(value = "是否删除(0-未删除,1-已删除)")
    protected boolean deleted;

}
