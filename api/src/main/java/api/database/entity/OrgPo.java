package api.database.entity;

import api.database.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import static common.constant.FieldConst.DASH;

/**
 * @author zys
 * @since 2023-04-28
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "OrgPo对象", description = "部门实例")
@TableName("info_org")
public class OrgPo extends BaseEntity implements Comparable<OrgPo> {

    @ApiModelProperty("父辈ID")
    private Long orgParentId;

    @ApiModelProperty("名称-本部门")
    private String orgName;

    @ApiModelProperty("名称-部门全路径 (使用`-`隔开)")
    private String orgFullName;

    @ApiModelProperty("父辈ID列表 (使用`,`隔开)")
    private String orgFullParent;

    @Override
    public int compareTo(OrgPo other) {
        return orgFullName.split(DASH).length - other.orgFullName.split(DASH).length;
    }

}
