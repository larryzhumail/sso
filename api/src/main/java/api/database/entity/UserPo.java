package api.database.entity;

import api.database.base.BaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author zys
 * @since 2023-04-21
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "UserPo对象", description = "用户实例")
@TableName("info_user")
public class UserPo extends BaseEntity {

    @ApiModelProperty("名称-显示用")
    private String userName;

    @ApiModelProperty("用户唯一标识-账户名")
    private String userAccount;

    @ApiModelProperty("密码-盐")
    private String userSalt;

    @ApiModelProperty("密码-加密密码")
    private String userPwd;

}
