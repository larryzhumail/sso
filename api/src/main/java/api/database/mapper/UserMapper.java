package api.database.mapper;

import api.database.entity.UserPo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zys
 * @since 2023-04-21
 */
public interface UserMapper extends BaseMapper<UserPo> {}
