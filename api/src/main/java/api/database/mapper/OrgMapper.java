package api.database.mapper;

import api.database.entity.OrgPo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @author zys
 * @since 2023-04-21
 */
public interface OrgMapper extends BaseMapper<OrgPo> {}
