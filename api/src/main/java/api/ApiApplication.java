package api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * @author zys
 * @since 2023-04-21
 */
@Slf4j
@SpringBootApplication(exclude = {RedisRepositoriesAutoConfiguration.class})
public class ApiApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(ApiApplication.class, args);
        log.info("(♥◠‿◠)ﾉﾞ  API模块启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                           " █████╗ ██████╗ ██╗      ██████╗ ██╗   ██╗███╗   ██╗███╗   ██╗██╗███╗   ██╗ ██████╗ \n" +
                           "██╔══██╗██╔══██╗██║      ██╔══██╗██║   ██║████╗  ██║████╗  ██║██║████╗  ██║██╔════╝ \n" +
                           "███████║██████╔╝██║█████╗██████╔╝██║   ██║██╔██╗ ██║██╔██╗ ██║██║██╔██╗ ██║██║  ███╗\n" +
                           "██╔══██║██╔═══╝ ██║╚════╝██╔══██╗██║   ██║██║╚██╗██║██║╚██╗██║██║██║╚██╗██║██║   ██║\n" +
                           "██║  ██║██║     ██║      ██║  ██║╚██████╔╝██║ ╚████║██║ ╚████║██║██║ ╚████║╚██████╔╝\n" +
                           "╚═╝  ╚═╝╚═╝     ╚═╝      ╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═══╝╚═╝  ╚═══╝╚═╝╚═╝  ╚═══╝ ╚═════╝ \n" +
                           "                                                                                    ");
    }

}
