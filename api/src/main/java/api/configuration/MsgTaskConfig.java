package api.configuration;

import api.task.MsgTask;
import common.constant.NameConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 延迟消息初始化器
 *
 * @author zys
 * @date 2023/05/12
 * @since 2023-05-12
 */
@Slf4j
@Configuration
public class MsgTaskConfig implements InitializingBean {

    @Resource(name = NameConst.MSG_TASK)
    private MsgTask msgTask;

    @Override
    public void afterPropertiesSet() {
        log.info("Init -> [MsgTaskConfig]");
        msgTask.start();
    }

}
