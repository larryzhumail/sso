package api.configuration;

import common.constant.NameConst;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author zys
 * @since 2023-04-25
 */
@Slf4j
@Configuration
@MapperScan(NameConst.MAPPER_PACKAGE_PATH)
@ComponentScan(basePackages = {"common.cache", "common.config", "common.base"})
public class Config {

    static {
        log.info("Init -> MapperScan -> 「api.database.mapper」");
        log.info("Init -> ComponentScan -> 「common.cache」「common.config」「common.base」");
    }

}
