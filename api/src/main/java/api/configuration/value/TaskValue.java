package api.configuration.value;

import common.constant.NameConst;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author zys
 * @since 2023-05-12
 */
@Data
@Component(NameConst.TASK_VALUE)
@ConfigurationProperties(prefix = NameConst.TASK_PREFIX)
public class TaskValue {

    private Boolean msg;

}
