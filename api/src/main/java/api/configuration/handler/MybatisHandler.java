package api.configuration.handler;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author zys
 * @since 2023-04-21
 */
@Component
public class MybatisHandler implements MetaObjectHandler {

    private static final String STATUS_FIELD_NAME = "status";
    private static final String CREATE_TIME_NAME = "createTime";
    private static final String UPDATE_TIME_NAME = "updateTime";

    @Override
    public void insertFill(MetaObject metaObject) {
        doInsert(metaObject, DateUtil.date());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        if (metaObject.hasGetter(UPDATE_TIME_NAME)) {
            strictUpdateFill(metaObject, UPDATE_TIME_NAME, Date.class, DateUtil.date());
        }
    }

    private void doInsert(MetaObject metaObject, Date time) {
        if (metaObject.hasGetter(STATUS_FIELD_NAME)) {
            strictInsertFill(metaObject, STATUS_FIELD_NAME, Integer.class, 1);
        }
        if (metaObject.hasGetter(CREATE_TIME_NAME)) {
            strictInsertFill(metaObject, CREATE_TIME_NAME, Date.class, time);
        }
    }

    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        // 分页插件
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(paginationInnerInterceptor());
        return interceptor;
    }

    /**
     * 分页插件，自动识别数据库类型
     * <a href="https://baomidou.com/guide/interceptor-pagination.html"> 文档 </a>
     */
    public PaginationInnerInterceptor paginationInnerInterceptor() {
        // 设置最大单页限制数量，默认 500 条，-1 不受限制
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        paginationInnerInterceptor.setMaxLimit(-1L);
        return paginationInnerInterceptor;
    }

}
