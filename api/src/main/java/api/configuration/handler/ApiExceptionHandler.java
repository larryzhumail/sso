package api.configuration.handler;

import common.base.result.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * @author zys
 * @since 2023-04-25
 */
@Slf4j
@RestControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler({Exception.class})
    public BaseResult exception(Exception exp) {
        // exp.printStackTrace();
        // log.error("{}", exp.getMessage());
        return BaseResult.fail(500, exp.getMessage());
    }

}
