package api.util;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import common.constant.NameConst;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * ID工具 (使用Mybatis ID 生成器)
 *
 * @author zys
 * @date 2023/05/06
 * @since 2023-05-06
 */
@SuppressWarnings("unused")
@Component(NameConst.ID_TOOL)
public class IdTool {

    private static final Object ENTITY = null;

    @Resource(type = IdentifierGenerator.class)
    private IdentifierGenerator generator;

    public Number nextNumber() {
        return generator.nextId(ENTITY);
    }

    public String nextUuid() {
        return generator.nextUUID(ENTITY);
    }

}
