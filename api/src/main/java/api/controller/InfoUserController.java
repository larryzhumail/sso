package api.controller;

import api.database.base.BaseController;
import api.database.entity.UserPo;
import api.service.UserService;
import common.base.annotation.LogAround;
import common.base.result.BaseResultData;
import common.custom.vo.InfoUserVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zys
 * @since 2023-04-22
 */
@Slf4j
@RestController
@RequestMapping("/v1/api/user")
@Api(value = "用户", tags = {"用户"})
public class InfoUserController extends BaseController<UserService, UserPo> {

    @LogAround
    @ApiOperation(value = "列表-获取")
    @GetMapping(value = "/list")
    public BaseResultData<List<InfoUserVo>> getUser() {
        return BaseResultData.success(service.getUsers());
    }

}
