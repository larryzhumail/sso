package api.controller;

import api.service.common.MsgService;
import common.base.annotation.LogAround;
import common.base.result.BaseResultData;
import common.constant.NameConst;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author zys
 * @since 2023-07-02
 */
@Slf4j
@RestController
@RequestMapping("/v1/api/msg")
@Api(value = "信息", tags = {"信息-测试"})
public class TestMsgController {

    @Resource(name = NameConst.MSG_SERVICE)
    private MsgService service;

    @LogAround
    @ApiOperation(value = "添加一个信息")
    @GetMapping(value = "/add")
    public BaseResultData<String> addMsg() {
        return BaseResultData.success(service.addMsg());
    }

    @LogAround
    @ApiOperation(value = "信息列表")
    @GetMapping(value = "/list")
    public BaseResultData<List<String>> msgList() {
        return BaseResultData.success(service.msgList());
    }

}
