package api.controller;

import api.database.base.BaseController;
import api.database.entity.OrgPo;
import api.service.OrgService;
import common.base.annotation.LogAround;
import common.base.result.BaseResultData;
import common.custom.form.InfoOrgForm;
import common.custom.vo.InfoOrgTreeNode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zys
 * @since 2023-05-05
 */
@Slf4j
@RestController
@RequestMapping("/v1/api/org")
@Api(value = "组织", tags = {"组织"})
public class InfoOrgController extends BaseController<OrgService, OrgPo> {

    @LogAround
    @ApiOperation(value = "保存")
    @PostMapping(value = "/save")
    public BaseResultData<Long> saveOrg(InfoOrgForm form) {
        return ok(service.saveOrg(form));
    }

    @LogAround
    @ApiOperation(value = "删除")
    @PostMapping(value = "/delete")
    public BaseResultData<Long> deleteOrg(InfoOrgForm form) {
        return ok(service.deleteOrg(form));
    }

    @LogAround
    @ApiOperation(value = "获取树")
    @GetMapping(value = "/tree")
    public BaseResultData<List<InfoOrgTreeNode>> getTree() {
        return ok(service.getTree());
    }

}
